/*
 Name:		LightControllerTest3.ino
 Created:	8/9/2019 8:36:03 AM
 Author:	b.obrien
*/



#include <Arduino.h>

//set up data objects


class SequenceData //this is a data object to store instances of the command requests from the can bus
	{
	public:
		int FunctionType = -1; //there will be several functions available from 1 to n more detail provided at sections
		int LightSet[33] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 }; // there are up to 16 pins/lights the last value should be 0
		double TimeTotal = -1;//this is the total desired time for the function to be completed in
		double PWMPercent = 0; //this would be used for a static light illumination
		int PWMMaxPercent = 100;//this is used for a variable light illumination
		int PWMMinPercent = 0;//this is used for a variable light illumination
		double StartTime = -1; // this is the recorded time the function started
		double TimeCount1 = -1; //this is the current time recorded at the start of each loop
		double TimeCount2 = -1; //This is the current time recorded at the end of each loop
		String Repeat = "No";//used to determine if the data should be retained and repeat the command so it is only sent once.
		int PWMwriteVal[33] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 }; //this is the variable used to write to the analogue pins
		int ChaseDirection = 0;//this variable is used to determine the default direction of the chase
		int PWMPointerI = 0; //This is used for the chase sequence pwm write so that only one pin is written to instead of all pins (This prevents the override of duplicate pins)
		int PWMPointerJ = 0; //This is used for the chase sequence pwm write so that only one pin is written to instead of all pins (This prevents override of duplicate pins)
		int DebugCommandNumber; //this variable is used to store the command number for debug

	};

//set up variables
int PWMNumberOfBits = 12;
int PWMValMax = (2 ^ PWMNumberOfBits);
double PWMFrequency;
int SequenceNumber = 0;
int PinsFrontHella[] = { 8, 7, 2,5,6,9, 10, 0 }; // front with hella
int PinsFront[] = { 8,7,9,10,0 };// front only
int PinsIndicatorAllHella[] = { 14,20,2,5,6,22,21,0 };//indicator with heela
int PinsIndicatorAll[] = { 21,22,20,14,0 };// indicator only
int PinsIndicatorRight[] = { 22,21,0 };
int PinsIndicatorLeft[] = { 20,14,0 };
int PinsBrakeAllHella[] = { 35,36,2,5,6,38,37,0 };//brake with hella
int PinsBrakeAll[] = { 35,36,38,37,0 }; //brake only
int PinsFootwell[] = { 29,0 };//footwell
int PinsHella[] = { 2,5,6,0 };//hela
int PinsAll[17] = { 8,7,9,10,21,22,20,14,35,36,38,37,29,2,5,6,0 }; //all


//used to debug available memory (check for memory leak)
//#ifdef __arm__
//// should use uinstd.h to define sbrk but Due causes a conflict
//extern "C" char* sbrk(int incr);
//#else  // __ARM__
//extern char* __brkval;
//#endif  // __arm__
//
//int freeMemory() {
//	char top;
//#ifdef __arm__
//	return &top - reinterpret_cast<char*>(sbrk(0));
//#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
//	return &top - __brkval;
//#else  // __arm__
//	return __brkval ? &top - __brkval : &top - __malloc_heap_start;
//#endif  // __arm__
//}

//set up the main sequence functions


class SequenceFunctions
{
	SequenceData Clean;

public:
	//Call main run function,
	//this creates and updated the time variables associated with the SequenceData instance
	//if the instance is 'clean' then the start time will be set
	//every 10 milliseconds the function 'sequence select' is called
	//within the sequence select are the various light effect calls
	// each light effect may be called by setting the 'SequenceNumber' to a value, the switch then selects the effect
	//Each effect returns a calculated pwm value which is passed back up to main loop
	//The pwm value for each instance of the SequenceData is then written to the pins.
	SequenceData RunSequence(SequenceData _sequencedata)
	{
		//Serial.println("RUN..........!");
		//Serial.print("Command = "); Serial.println(_sequencedata.DebugCommandNumber);

		//set the start time of the sequence this is used to determine how far into the sequence the software is on each loop
		if ((_sequencedata.StartTime) == (-1.00))
		{
			//Serial.println("Start Time......!");
			_sequencedata.StartTime = micros();
			_sequencedata.TimeCount2 = _sequencedata.StartTime;

		}

		//for the time wrap around simply add on the current time read to the last one, this prevents invalid pwm values being generated causing the program to appear to freeze.
		//if no time wrap around (as in the start time is less than the current read time) then normal update.
		if (_sequencedata.StartTime > micros())
		{
			_sequencedata.TimeCount1 = _sequencedata.TimeCount1 + micros();
		}
		else
		{
			_sequencedata.TimeCount1 = micros();
		}

		//Serial.print("BoardTime = "); Serial.println(micros());
		//Serial.print("10 milli calc = TimeCount 1("); Serial.print(_sequencedata.TimeCount1); Serial.print(")-TimeCount2("); Serial.print(_sequencedata.TimeCount2); Serial.print(")="); Serial.println(_sequencedata.TimeCount1 - _sequencedata.TimeCount2);
		//when 10 milliseconds or more has passed update the pwm value being written to the pins
		if ((_sequencedata.TimeCount1 - _sequencedata.TimeCount2) >= 10000)
		{
			//Serial.println("10 Millisecond......!");

			_sequencedata.TimeCount2 = _sequencedata.TimeCount1;//as 10millisecons has passed store the time stamp
			_sequencedata = SequenceSelect(_sequencedata);//run this method

		}

		//when the sequence is completed replace the cmd values with the default
		if (((_sequencedata.TimeCount1 - _sequencedata.StartTime) >= _sequencedata.TimeTotal))
		{
			//Serial.println("Repeat......!");
			_sequencedata.StartTime = -1;

			if (_sequencedata.Repeat == "No")
			{
				_sequencedata = Clean;
			}
		}

		return _sequencedata;
	}


private:

	SequenceData SequenceSelect(SequenceData _sequencedata)
	{

		switch (_sequencedata.FunctionType)
		{
		case -1:
			_sequencedata = Clean;
			//Serial.println("Sequence Clean");
		case 1:
			//Serial.println("Sequence Fade up");
			return FadeUp(_sequencedata);
		case 2:
			//Serial.println("Sequence Fade down");
			return FadeDown(_sequencedata);
		case 3:
			//Serial.println("Sequence FadeUpDown");
			return FadeUpDown(_sequencedata);
			//Serial.println("FadeUpDown Return");
		case 4:
			//Serial.println("Sequence FadeDownUp");
			return FadeDownUp(_sequencedata);
		case 5:
			//Serial.println("Sequence Chase");
			return Chase(_sequencedata);

			break;

		}
		return _sequencedata;
	}

	//set up software functions
	int ConvertPercentageToPWM(int Percentage)
	{
		int L = (pow(2, PWMNumberOfBits) - 1) * (Percentage / 100);
		return L;
	}

	int GammaCorrection(double PWMVal)//this is used for all functions except chase
	{
		int PWMOut = round((pow(2, PWMNumberOfBits) - 1) * pow((PWMVal / (pow(2, PWMNumberOfBits) - 1)), 2.8));
		return PWMOut;
	}
	int GammaCorrection2(double PWMVal)//this is used for chase sequences to smooth out transition between LED's
	{
		int PWMOut = round((pow(2, PWMNumberOfBits) - 1) * pow((PWMVal / (pow(2, PWMNumberOfBits) - 1)), 1.7));
		return PWMOut;
	}

	SequenceData FadeUp(SequenceData _sequencedata)
	{


		//convert the desired percentage values into PWM values
		double MaxPVal = ConvertPercentageToPWM(_sequencedata.PWMMaxPercent);
		double MinPVal = ConvertPercentageToPWM(_sequencedata.PWMMinPercent);

		//calculate the current pwm level to be used based on time lapsed so far in sequence
		double PWM = MinPVal + ((MaxPVal - MinPVal) / (_sequencedata.TimeTotal / 10000)) * round((_sequencedata.TimeCount1 - _sequencedata.StartTime) / 10000);
		int PWMOut = GammaCorrection(PWM);
		//Serial.println("Fade Up");

		//Serial.print("PWM1 = "); Serial.println(PWM);
		//Serial.print("fade up pwmout= "); Serial.println(PWMOut); Serial.println();
		for (int i = 0; i < 16; i++)
		{
			_sequencedata.PWMwriteVal[i] = PWMOut;
		}
		//perform PWM
		return _sequencedata;
	}

	//PWM fade down
	SequenceData FadeDown(SequenceData _sequencedata)
	{

		//convert the desired percentage values into PWM values
		int MaxPVal = ConvertPercentageToPWM(_sequencedata.PWMMaxPercent);
		int MinPVal = ConvertPercentageToPWM(_sequencedata.PWMMinPercent);

		//calculate the current pwm level to be used based on time lapsed so far in sequence
		int PWM = MaxPVal - ((MaxPVal - MinPVal) / (_sequencedata.TimeTotal / 10000)) * ((_sequencedata.TimeCount1 - _sequencedata.StartTime) / 10000);
		int PWMOut = GammaCorrection(PWM);
		//Serial.println("Fade Down");
		//
		//Serial.print("PWM1 = "); Serial.println(PWM);
		//Serial.print("fade down pwmout= "); Serial.println(PWMOut); Serial.println();
		for (int i = 0; i < 16; i++)
		{
			_sequencedata.PWMwriteVal[i] = PWMOut;
		}
		return _sequencedata;
	};

	// fade up followed by fade down both at same time interval
	SequenceData FadeUpDown(SequenceData _sequencedata)
	{

		double MaxPVal = ConvertPercentageToPWM(_sequencedata.PWMMaxPercent);
		double MinPVal = ConvertPercentageToPWM(_sequencedata.PWMMinPercent);
		//TimeLapse is used to determine the time lapsed so far in the sequence runtime, 
		//this is then used to determine the usage of each component of the function
		double TimeLapse = (_sequencedata.TimeCount1 - _sequencedata.StartTime);
		//TimeTotal is used for each individual section within the sequence
		//there are 2 sections within this sequence
		double TimeTotal = (_sequencedata.TimeTotal / 2);


		if (TimeLapse < TimeTotal)
		{	//calculate the current pwm level to be used based on time lapsed so far in sequence
			double PWM = (MinPVal + (((MaxPVal - MinPVal) / (TimeTotal / 10000)) * round(TimeLapse / 10000)));

			int PWMOut = GammaCorrection(PWM);
			//Serial.println("FadeUpDown");
			//Serial.print("TimeLapse = "); Serial.println(TimeLapse);
			//Serial.print("PWM1 = "); Serial.println(PWM);
			//Serial.print("fade Up pwmout= "); Serial.println(PWMOut); Serial.println();
			for (int i = 0; i < 16; i++)
			{
				_sequencedata.PWMwriteVal[i] = PWMOut;
			}

			return _sequencedata;
		}

		if (TimeLapse > TimeTotal)
		{	//calculate the current pwm level to be used based on time lapsed so far in sequence

			double PWM = (MaxPVal - (((MaxPVal - MinPVal) / (TimeTotal / 10000)) * round((TimeLapse - TimeTotal) / 10000)));
			int PWMOut = GammaCorrection(PWM);
			//Serial.println("FadeUpDown");
			//Serial.print("TimeLapse = "); Serial.println(TimeLapse);
			//Serial.print("PWM1 = "); Serial.println(PWM);
			//Serial.print("fade Down pwmout= "); Serial.println(PWMOut); Serial.println();
			for (int i = 0; i < 16; i++)
			{
				_sequencedata.PWMwriteVal[i] = PWMOut;
			}

			return _sequencedata;

		}
		return _sequencedata;
	}

	SequenceData FadeDownUp(SequenceData _sequencedata) //fade down followed by fade up, both at same time interval
	{
		double MaxPVal = ConvertPercentageToPWM(_sequencedata.PWMMaxPercent);
		double MinPVal = ConvertPercentageToPWM(_sequencedata.PWMMinPercent);
		//TimeLapse is used to determine the time lapsed so far in the sequence runtime, 
		//this is then used to determine the usage of each component of the function
		double TimeLapse = (_sequencedata.TimeCount1 - _sequencedata.StartTime);
		//TimeTotal is used for each individual section within the sequence
		//there are 2 sections within this sequence
		double TimeTotal = (_sequencedata.TimeTotal / 2);


		if (TimeLapse < TimeTotal)
		{	//calculate the current pwm level to be used based on time lapsed so far in sequence

			double PWM = (MaxPVal - ((MaxPVal - MinPVal) / (TimeTotal / 10000)) * round((TimeLapse) / 10000));


			int PWMOut = GammaCorrection(PWM);
			//Serial.println("FadeDownUp");
			//Serial.print("TimeLapse = "); Serial.println(TimeLapse);
			//Serial.print("PWM1 = "); Serial.println(PWM);
			//Serial.print("fade Down pwmout= "); Serial.println(PWMOut); Serial.println();
			for (int i = 0; i < 16; i++)
			{
				_sequencedata.PWMwriteVal[i] = PWMOut;
			}

			return _sequencedata;

		}
		if (TimeLapse > TimeTotal)
		{	//calculate the current pwm level to be used based on time lapsed so far in sequence

			double PWM = (MinPVal + ((MaxPVal - MinPVal) / (TimeTotal / 10000)) * round((TimeLapse - TimeTotal) / 10000));

			int PWMOut = GammaCorrection(PWM);
			//Serial.println("FadeDownUp");
			//Serial.print("TimeLapse = "); Serial.println(TimeLapse);
			//Serial.print("PWM1 = "); Serial.println(PWM);
			//Serial.print("fade Up pwmout= "); Serial.println(PWMOut); Serial.println();
			for (int i = 0; i < 16; i++)
			{
				_sequencedata.PWMwriteVal[i] = PWMOut;
			}

			return _sequencedata;
		}
		return _sequencedata;
	}

	SequenceData Chase(SequenceData _sequencedata)
	{
		//work out the number of elements in Pin[], loop through pin until 0 is found. 0 must be avoided as a pin number otherwise it doesnt work.
		int x = 0;
		while (_sequencedata.LightSet[x] != 0)
		{
			x++;
		}

		double MaxPVal = ConvertPercentageToPWM(_sequencedata.PWMMaxPercent);
		double MinPVal = ConvertPercentageToPWM(_sequencedata.PWMMinPercent);


		//TimeLapse is used to determine the time lapsed so far in the sequence runtime, 
		//this is then used to determine the usage of each component of the function
		double TimeLapse = (_sequencedata.TimeCount1 - _sequencedata.StartTime);
		//double InverseTimeLapse = (_sequencedata.StartTime + _sequencedata.TimeTotal - TimeLapse);

		//This is used to calculate the total number of time intervals and is then used to calculate the
		//pin number being faded up or down
		double TimeIntervals;


		//i and j are used as the position in the pins array for fade up and down
		int i;
		int j;

		double PWM1;
		double PWM2;

		double PWMConstant;


		//run a positive chase from pin array 0 to x
		if (_sequencedata.ChaseDirection == 0)
		{
			TimeIntervals = (_sequencedata.TimeTotal / (x + 1));
			i = (TimeLapse / (TimeIntervals));
			j = ((TimeLapse - (TimeIntervals)) / TimeIntervals);
			if (i==0)
			{
				j = -1;
			}
			PWMConstant = (((MaxPVal - MinPVal) / (TimeIntervals / 10000)) * ((TimeLapse - (i * TimeIntervals)) / 10000));

		}
		//run a negative chase from pin array x to 0
		if (_sequencedata.ChaseDirection == 1)
		{
			TimeIntervals = (_sequencedata.TimeTotal / (x + 1));
			i = x - (TimeLapse / TimeIntervals);
			j = x - ((TimeLapse - TimeIntervals) / TimeIntervals);
			int k = (TimeLapse / TimeIntervals);
			
			

			PWMConstant = (((MaxPVal - MinPVal) / (TimeIntervals / 10000)) * ((TimeLapse - (k * TimeIntervals)) / 10000));

		}

		//Perform a continuous +ve to -ve chase equence
		if (_sequencedata.ChaseDirection == 2)
		{

			//This is used for a +ve followed by a -ve chase sequence or vice versa 
			//all time intervals are literally halved as the number of pwm values doubles
			TimeIntervals = (_sequencedata.TimeTotal / (2*(x-1)));
			

			if (TimeLapse < (_sequencedata.TimeTotal/2))
			{

				i = ((TimeLapse )/ TimeIntervals) ;
				j = ((TimeLapse - (TimeIntervals)) / TimeIntervals);
				if (i == 0)
				{
					j = 1;
				}

				PWMConstant = (((MaxPVal - MinPVal) / (TimeIntervals / 10000)) * ((TimeLapse - (i * TimeIntervals)) / 10000));

			}
			if (TimeLapse > (_sequencedata.TimeTotal / 2))
			{
			
				i = x - ((TimeLapse - (_sequencedata.TimeTotal / 2)) / TimeIntervals) ;
				j = x + 0 - ((TimeLapse - (_sequencedata.TimeTotal / 2) - TimeIntervals) / TimeIntervals) ;
				int k = ((TimeLapse - (_sequencedata.TimeTotal / 2)) / TimeIntervals);

				if (j==x)
				{
					j = x - 2;
				}

				PWMConstant = (((MaxPVal - MinPVal) / (TimeIntervals / 10000)) * (((TimeLapse - (_sequencedata.TimeTotal / 2)) - (k * TimeIntervals)) / 10000));
				
			}

		}

		{
			//fade up the first pin

			PWM1 = (MinPVal + PWMConstant);

			int PWMOut1 = GammaCorrection2(PWM1);

			Serial.print("Timeintervals = "); Serial.println(TimeIntervals);
			Serial.print("TimeLapse = "); Serial.println(TimeLapse);
			Serial.print("array number i = "); Serial.println(i);
			Serial.print("array[i] value ="); Serial.println(_sequencedata.LightSet[i]);
			Serial.print("PWM1 = "); Serial.println(PWM1);
			Serial.print("fadeUp chase pwmout= "); Serial.println(PWMOut1);

			_sequencedata.PWMPointerI = i;
			_sequencedata.PWMwriteVal[i] = PWMOut1;
		}
		{
			//fade down the previous pin

			PWM2 = ((MaxPVal - PWMConstant));

			int PWMOut2 = GammaCorrection2(PWM2);

			Serial.print("array number j = "); Serial.println(j);
			Serial.print("array[j] value ="); Serial.println(_sequencedata.LightSet[j]);
			Serial.print("PWM2 = "); Serial.println(PWM2);
			Serial.print("fadeDown chase pwmout= "); Serial.println(PWMOut2); Serial.println();

			if (PWMOut2 <= 400)
			{
				PWMOut2 = 0;
			}
			_sequencedata.PWMPointerJ = j;
			_sequencedata.PWMwriteVal[j] = PWMOut2;
		}

		return _sequencedata;
	}

};


//set up unique instances of the the functional code to run multiple light effects at the same time
//16 sequences so that each light can be controlled individually
SequenceFunctions SF1;
SequenceFunctions SF2;
SequenceFunctions SF3;
SequenceFunctions SF4;
SequenceFunctions SF5;
SequenceFunctions SF6;
SequenceFunctions SF7;
SequenceFunctions SF8;
SequenceFunctions SF9;
SequenceFunctions SF10;
SequenceFunctions SF11;
SequenceFunctions SF12;
SequenceFunctions SF13;
SequenceFunctions SF14;
SequenceFunctions SF15;
SequenceFunctions SF16;

//set up command data sets
//It is possible to control each individual light with 16 commands
SequenceData cmd1;
SequenceData cmd2;
SequenceData cmd3;
SequenceData cmd4;
SequenceData cmd5;
SequenceData cmd6;
SequenceData cmd7;
SequenceData cmd8;
SequenceData cmd9;
SequenceData cmd10;
SequenceData cmd11;
SequenceData cmd12;
SequenceData cmd13;
SequenceData cmd14;
SequenceData cmd15;
SequenceData cmd16;


int roundup(float Input)
{
	if (Input > round(Input))
	{
		Input = round(Input) + 1;
	}
	else
	{
		Input = round(Input);
	}
	return Input;
}

void _pwmWrite(int LightSet[],int PWMWriteVal[],int i, int j, int FunctionType, int ChaseDirection )//external analogue write as this retains the pwm value?
{

	if ((FunctionType == 5) && ((ChaseDirection == 2)||(ChaseDirection==20)))
	{
		//only write to the sequences current pin this prevents overwriting of duplicated pins
		analogWrite(LightSet[i], PWMWriteVal[i]);
		analogWrite(LightSet[j], PWMWriteVal[j]);

	}
	else
	{


		//Serial.println("PWMwrite");
		//work out the number of elements in Pin[], loop through pin until 0 is found. 0 must be avoided as a pin number otherwise it doesnt work.
		int x = 0;
		while (LightSet[x] != 0)
		{
			x++;
		}
		int PinSize = x;

		for (int n = 0; n < PinSize; n++)
		{
			analogWrite(LightSet[n], PWMWriteVal[n]);// for all the pins in the set correct the gamma level and set the pwm value for the pin

		}
	}

}


// the setup function runs once when you press reset or power the board
void setup()
{
	//set the frequency based on the number of required PWM bits
	analogWriteResolution(PWMNumberOfBits);
	SequenceNumber = 0;
	Serial.begin(115200);


	//test set up only
	cmd1.FunctionType = 3;
	cmd1.LightSet[0] = 8;
	cmd1.LightSet[1] = 7;
	cmd1.LightSet[2] = 9;
	cmd1.LightSet[3] = 10;
	cmd1.PWMMaxPercent = 100;
	cmd1.PWMMinPercent = 0;
	cmd1.Repeat = "Yes";
	cmd1.TimeTotal = 3 * 1000 * 1000; //micro secconds
	cmd1.DebugCommandNumber = 1;
	cmd1.ChaseDirection = 2;

	cmd2.FunctionType = 4;
	cmd2.LightSet[0] = 21;
	cmd2.LightSet[1] = 22;
	cmd2.LightSet[2] = 20;
	cmd2.LightSet[3] = 14;
	cmd2.PWMMaxPercent = 100;
	cmd2.PWMMinPercent = 0;
	cmd2.Repeat = "Yes";
	cmd2.TimeTotal = 1.5 * 1000 * 1000; //3 seconds multiplied to get Milli then micro
	cmd2.DebugCommandNumber = 2;

	cmd3.FunctionType = 3;
	cmd3.LightSet[0] = 35;
	cmd3.LightSet[1] = 36;
	cmd3.LightSet[2] = 38;
	cmd3.LightSet[3] = 37;
	cmd3.PWMMaxPercent = 100;
	cmd3.PWMMinPercent = 0;
	cmd3.Repeat = "Yes";
	cmd3.TimeTotal = 1.5 * 1000 * 1000; //3 seconds multiplied to get Milli then micro
	cmd3.DebugCommandNumber = 3;

	cmd4.FunctionType = 5;
	cmd4.LightSet[0] = 2;
	cmd4.LightSet[1] = 5;
	cmd4.LightSet[2] = 6;
	//cmd4.LightSet[0] = 8;
	//cmd4.LightSet[1] = 7;
	//cmd4.LightSet[5] = 9;
	//cmd4.LightSet[6] = 10;
	//cmd4.LightSet[7] = 9;
	//cmd4.LightSet[8] = 6;
	//cmd4.LightSet[9] = 5;
	//cmd4.LightSet[10] = 2;
	//cmd4.LightSet[11] = 7;
	//cmd4.LightSet[12] = 8;
	//cmd4.PWMwriteVal[0] = 4095;
	//cmd4.PWMwriteVal[1] = 4095;
	//cmd4.PWMwriteVal[2] = 4095;
	cmd4.PWMMaxPercent = 100;
	cmd4.PWMMinPercent = 0;
	cmd4.Repeat = "Yes";
	cmd4.TimeTotal = 1.5 * 1000 * 1000; //3 seconds multiplied to get Milli then micro
	cmd4.ChaseDirection = 2;
	cmd4.DebugCommandNumber = 4;

	cmd5.FunctionType = 0;
	cmd5.LightSet[0] = 29;
	cmd5.PWMwriteVal[0] = 4095;
	cmd5.Repeat = "Yes";
	cmd5.DebugCommandNumber = 5;



}

// the loop function runs over and over again until power down or reset
void loop()
{
	//Serial.println("!...........START..........!");

	//debug stuff
	cmd6.DebugCommandNumber = 6;
	cmd7.DebugCommandNumber = 7;
	cmd8.DebugCommandNumber = 8;
	cmd9.DebugCommandNumber = 9;
	cmd10.DebugCommandNumber = 10;
	cmd11.DebugCommandNumber = 11;
	cmd12.DebugCommandNumber = 12;
	cmd13.DebugCommandNumber = 13;
	cmd14.DebugCommandNumber = 14;
	cmd15.DebugCommandNumber = 15;
	cmd16.DebugCommandNumber = 16;

	cmd1 = SF1.RunSequence(cmd1);
	cmd2 = SF2.RunSequence(cmd2);
	cmd3 = SF2.RunSequence(cmd3);
	cmd4 = SF2.RunSequence(cmd4);
	cmd5 = SF2.RunSequence(cmd5);
	cmd6 = SF2.RunSequence(cmd6);
	cmd7 = SF2.RunSequence(cmd7);
	cmd8 = SF2.RunSequence(cmd8);
	cmd9 = SF2.RunSequence(cmd9);
	cmd10 = SF2.RunSequence(cmd10);
	cmd11 = SF2.RunSequence(cmd11);
	cmd12 = SF2.RunSequence(cmd12);
	cmd13 = SF2.RunSequence(cmd13);
	cmd14 = SF2.RunSequence(cmd14);
	cmd15 = SF2.RunSequence(cmd15);
	cmd16 = SF2.RunSequence(cmd16);


	_pwmWrite(cmd1.LightSet,cmd1.PWMwriteVal,cmd1.PWMPointerI,cmd1.PWMPointerJ,cmd1.FunctionType,cmd1.ChaseDirection);
	_pwmWrite(cmd2.LightSet, cmd2.PWMwriteVal, cmd2.PWMPointerI, cmd2.PWMPointerJ, cmd2.FunctionType, cmd2.ChaseDirection);
	_pwmWrite(cmd3.LightSet, cmd3.PWMwriteVal, cmd3.PWMPointerI, cmd3.PWMPointerJ, cmd3.FunctionType, cmd3.ChaseDirection);
	_pwmWrite(cmd4.LightSet, cmd4.PWMwriteVal, cmd4.PWMPointerI, cmd4.PWMPointerJ, cmd4.FunctionType, cmd4.ChaseDirection);
	_pwmWrite(cmd5.LightSet, cmd5.PWMwriteVal, cmd5.PWMPointerI, cmd5.PWMPointerJ, cmd5.FunctionType, cmd5.ChaseDirection);
	_pwmWrite(cmd6.LightSet, cmd6.PWMwriteVal, cmd6.PWMPointerI, cmd6.PWMPointerJ, cmd6.FunctionType, cmd6.ChaseDirection);
	_pwmWrite(cmd7.LightSet, cmd7.PWMwriteVal, cmd7.PWMPointerI, cmd7.PWMPointerJ, cmd7.FunctionType, cmd7.ChaseDirection);
	_pwmWrite(cmd8.LightSet, cmd8.PWMwriteVal, cmd8.PWMPointerI, cmd8.PWMPointerJ, cmd8.FunctionType, cmd8.ChaseDirection);
	_pwmWrite(cmd9.LightSet, cmd9.PWMwriteVal, cmd9.PWMPointerI, cmd9.PWMPointerJ, cmd9.FunctionType, cmd9.ChaseDirection);
	_pwmWrite(cmd10.LightSet, cmd10.PWMwriteVal, cmd10.PWMPointerI, cmd10.PWMPointerJ, cmd10.FunctionType, cmd10.ChaseDirection);
	_pwmWrite(cmd11.LightSet, cmd11.PWMwriteVal, cmd11.PWMPointerI, cmd11.PWMPointerJ, cmd11.FunctionType, cmd11.ChaseDirection);
	_pwmWrite(cmd12.LightSet, cmd12.PWMwriteVal, cmd12.PWMPointerI, cmd12.PWMPointerJ, cmd12.FunctionType, cmd12.ChaseDirection);
	_pwmWrite(cmd13.LightSet, cmd13.PWMwriteVal, cmd13.PWMPointerI, cmd13.PWMPointerJ, cmd13.FunctionType, cmd13.ChaseDirection);
	_pwmWrite(cmd14.LightSet, cmd14.PWMwriteVal, cmd14.PWMPointerI, cmd14.PWMPointerJ, cmd14.FunctionType, cmd14.ChaseDirection);
	_pwmWrite(cmd15.LightSet, cmd15.PWMwriteVal, cmd15.PWMPointerI, cmd15.PWMPointerJ, cmd15.FunctionType, cmd15.ChaseDirection);
	_pwmWrite(cmd16.LightSet, cmd16.PWMwriteVal, cmd16.PWMPointerI, cmd16.PWMPointerJ, cmd16.FunctionType, cmd16.ChaseDirection);

	//Serial.println("!...........END..........!");

}
