/*
 Name:		LightControllerTest3.ino
 Created:	8/9/2019 8:36:03 AM
 Author:	b.obrien
*/


#include <kinetis_flexcan.h>
#include <FlexCAN.h>
#include <Arduino.h>

//set up data objects


class SequenceData //this is a data object to store instances of the command requests from the can bus
{
public:
	int FunctionType =-1; //there will be several functions available from 1 to n more detail provided at sections
	int LightSet[17] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; // there are up to 16 pins/lights the last value should be 0
	double TimeTotal = -1;//this is the total desired time for the function to be completed in
	double PWMPercent = -0; //this would be used for a static light illumination
	double PWMMaxPercent = 100;//this is used for a variable light illumination
	double PWMMinPercent = 0;//this is used for a variable light illumination
	double StartTime = -1; // this is the recorded time the function started
	double TimeCount1 = -1; //this is the current time recorded at the start of each loop
	double TimeCount2 = -1; //This is the current time recorded at the end of each loop
	String Repeat = "No" ;//used to determine if the data should be retained and repeat the command so it is only sent once.
	int PWMwriteVal[17] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 }; //this is the variable used to write to the analogue pins
	int ChaseDirection = 0;//this variable is used to determine the default direction of the chase
	int ChasePosition = 0; //this variable is used to store the position in the chase sequence (more specifically the pin that has pwm being set)
};

//set up variables
int PWMNumberOfBits = 12;
int PWMValMax = (2 ^ PWMNumberOfBits);
double PWMFrequency;
int SequenceNumber = 0;
int PinsFrontHella[] = { 8, 7, 2,5,6,9, 10, 0 }; // front with hella
int PinsFront[] = { 8,7,9,10,0 };// front only
int PinsIndicatorAllHella[] = { 14,20,2,5,6,22,21,0 };//indicator with heela
int PinsIndicatorAll[] = { 21,22,20,14,0 };// indicator only
int PinsIndicatorRight[] = { 22,21,0 };
int PinsIndicatorLeft[] = { 20,14,0 };
int PinsBrakeAllHella[] = { 35,36,2,5,6,38,37,0 };//brake with hella
int PinsBrakeAll[] = { 35,36,38,37,0 }; //brake only
int PinsFootwell[] = { 29,0 };//footwell
int PinsHella[] = { 2,5,6,0 };//hela
int PinsAll[17] = { 8,7,9,10,21,22,20,14,35,36,38,37,29,2,5,6,0 }; //all


//used to debug available memory (check for memory leak)
//#ifdef __arm__
//// should use uinstd.h to define sbrk but Due causes a conflict
//extern "C" char* sbrk(int incr);
//#else  // __ARM__
//extern char* __brkval;
//#endif  // __arm__
//
//int freeMemory() {
//	char top;
//#ifdef __arm__
//	return &top - reinterpret_cast<char*>(sbrk(0));
//#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
//	return &top - __brkval;
//#else  // __arm__
//	return __brkval ? &top - __brkval : &top - __malloc_heap_start;
//#endif  // __arm__
//}

//set up the main sequence functions


class SequenceFunctions
{
	SequenceData Clean;

public:
	//Call main run function,
	//this creates and updated the time variables associated with the SequenceData instance
	//if the instance is 'clean' then the start time will be set
	//every 10 milliseconds the function 'sequence select' is called
	//within the sequence select are the various light effect calls
	// each light effect may be called by setting the 'SequenceNumber' to a value, the switch then selects the effect
	//Each effect returns a calculated pwm value which is passed back up to main loop
	//The pwm value for each instance of the SequenceData is then written to the pins.
	SequenceData RunSequence(SequenceData _sequencedata)
	{

		//set the start time of the sequence this is used to determine how far into the sequence the software is on each loop
		if ((_sequencedata.StartTime) == (-1.00))
		{
			_sequencedata.StartTime = micros();
			_sequencedata.TimeCount2 = _sequencedata.StartTime;

		}
		_sequencedata.TimeCount1 = micros();


		//when 10 milliseconds or more has passed update the pwm value being written to the pins
		if ((_sequencedata.TimeCount1 - _sequencedata.TimeCount2) >= 10000)
		{
			_sequencedata.TimeCount2 = _sequencedata.TimeCount1;//as 10millisecons has passed store the time stamp
			_sequencedata = SequenceSelect(_sequencedata);//run this method

		}

		//when the sequence is completed replace the cmd values with the default
		if (((_sequencedata.TimeCount1 - _sequencedata.StartTime) >= _sequencedata.TimeTotal))
		{
			_sequencedata.StartTime = micros();

			if (_sequencedata.Repeat == "No")
			{
				_sequencedata = Clean;
			}
		}

		return _sequencedata;
	}


private:

	SequenceData SequenceSelect(SequenceData _sequencedata)
	{

		switch (_sequencedata.FunctionType)
		{
		case -1:
			_sequencedata = Clean;
			Serial.println("Sequence Clean");
		case 1:
			Serial.println("Sequence Fade up");
			return FadeUp(_sequencedata);
		case 2:
			Serial.println("Sequence Fade down");
			return FadeDown(_sequencedata);
		case 3:
			Serial.println("Sequence FadeUpDown");
			return FadeUpDown(_sequencedata);
		case 4:
			Serial.println("Sequence FadeDownUp");
			return FadeDownUp(_sequencedata);
		case 5:
			Serial.println("Sequence Chase");
			return Chase(_sequencedata);

			break;

		}
		return _sequencedata;
	}

	//set up software functions
	int ConvertPercentageToPWM(double Percentage)
	{
		int L = (pow(2, PWMNumberOfBits) - 1) * (Percentage / 100);
		return L;
	}

	int GammaCorrection(double PWMVal)
	{
		int PWMOut = (pow(2, PWMNumberOfBits) - 1) * pow((PWMVal / (pow(2, PWMNumberOfBits) - 1)), 2.8);
		return PWMOut;
	}

	SequenceData FadeUp(SequenceData _sequencedata)
	{


		//convert the desired percentage values into PWM values
		double MaxPVal = ConvertPercentageToPWM(_sequencedata.PWMMaxPercent);
		double MinPVal = ConvertPercentageToPWM(_sequencedata.PWMMinPercent);

		//calculate the current pwm level to be used based on time lapsed so far in sequence
		double PWM = MinPVal + ((MaxPVal - MinPVal) / (_sequencedata.TimeTotal / 10000)) * round((_sequencedata.TimeCount1 - _sequencedata.StartTime) / 10000);
		int PWMOut = GammaCorrection(PWM);
		for (int i = 0; i <= 17; i++)
		{
			_sequencedata.PWMwriteVal[i] = PWMOut;
		}
		//perform PWM

	}

	//PWM fade down
	SequenceData FadeDown(SequenceData _sequencedata)
	{

		//convert the desired percentage values into PWM values
		int MaxPVal = ConvertPercentageToPWM(_sequencedata.PWMMaxPercent);
		int MinPVal = ConvertPercentageToPWM(_sequencedata.PWMMinPercent);

		//calculate the current pwm level to be used based on time lapsed so far in sequence
		int PWM = MaxPVal - ((MaxPVal - MinPVal) / (_sequencedata.TimeTotal / 10000)) * ((_sequencedata.TimeCount1 - _sequencedata.StartTime) / 10000);
		int PWMOut = GammaCorrection(PWM);
		for (int i = 0; i <= 17; i++)
		{
			_sequencedata.PWMwriteVal[i] = PWMOut;
		}

	};

	SequenceData FadeUpDown(SequenceData _sequencedata) // fade up followed by fade down both at same time interval
	{
		double MaxPVal = ConvertPercentageToPWM(_sequencedata.PWMMaxPercent);
		double MinPVal = ConvertPercentageToPWM(_sequencedata.PWMMinPercent);
		//TimeLapse is used to determine the time lapsed so far in the sequence runtime, 
		//this is then used to determine the usage of each component of the function
		double TimeLapse = (_sequencedata.TimeCount1 - _sequencedata.StartTime);
		//TimeTotal is used for each individual section within the sequence
		//there are 2 sections within this sequence
		double TimeTotal = (_sequencedata.TimeTotal / 2);


		if (TimeLapse < TimeTotal)
		{	//calculate the current pwm level to be used based on time lapsed so far in sequence
			double PWM = (MinPVal + (((MaxPVal - MinPVal) / (TimeTotal / 10000)) * round(TimeLapse / 10000)));

			int PWMOut = GammaCorrection(PWM);
			for (int i = 0; i <= 17; i++)
			{
				_sequencedata.PWMwriteVal[i] = PWMOut;
			}

			return _sequencedata;
		}

		if (TimeLapse > TimeTotal)
		{	//calculate the current pwm level to be used based on time lapsed so far in sequence

			double PWM = (MaxPVal - (((MaxPVal - MinPVal) / (TimeTotal / 10000)) * round((TimeLapse - TimeTotal) / 10000)));
			int PWMOut = GammaCorrection(PWM);
			for (int i = 0; i <= 17; i++)
			{
				_sequencedata.PWMwriteVal[i] = PWMOut;
			}

			return _sequencedata;

		}

	}

	SequenceData FadeDownUp(SequenceData _sequencedata) //fade down followed by fade up, both at same time interval
	{
		double MaxPVal = ConvertPercentageToPWM(_sequencedata.PWMMaxPercent);
		double MinPVal = ConvertPercentageToPWM(_sequencedata.PWMMinPercent);
		//TimeLapse is used to determine the time lapsed so far in the sequence runtime, 
		//this is then used to determine the usage of each component of the function
		double TimeLapse = (_sequencedata.TimeCount1 - _sequencedata.StartTime);
		//TimeTotal is used for each individual section within the sequence
		//there are 2 sections within this sequence
		double TimeTotal = (_sequencedata.TimeTotal / 2);
		

		if (TimeLapse < TimeTotal)
		{	//calculate the current pwm level to be used based on time lapsed so far in sequence

			double PWM = (MaxPVal - ((MaxPVal - MinPVal) / (TimeTotal / 10000)) * round((TimeLapse) / 10000));

			int PWMOut = GammaCorrection(PWM);
			for (int i = 0; i <= 17; i++)
			{
				_sequencedata.PWMwriteVal[i] = PWMOut;
			}

			return _sequencedata;

		}
		if (TimeLapse > TimeTotal)
		{	//calculate the current pwm level to be used based on time lapsed so far in sequence

			double PWM = (MinPVal + ((MaxPVal - MinPVal) / (TimeTotal / 10000)) * round((TimeLapse - TimeTotal) / 10000));

			int PWMOut = GammaCorrection(PWM);
			for (int i = 0; i <= 17; i++)
			{
				_sequencedata.PWMwriteVal[i] = PWMOut;
			}

			return _sequencedata;
		}

	}

	SequenceData Chase(SequenceData _sequencedata)
	{
		//work out the number of elements in Pin[], loop through pin until 0 is found. 0 must be avoided as a pin number otherwise it doesnt work.
		int x = 0;
		while (_sequencedata.LightSet[x] != 0)
		{
			x++;
		}
		int NoPins = x;




		//TimeLapse is used to determine the time lapsed so far in the sequence runtime, 
		//this is then used to determine the usage of each component of the function
		double TimeLapse = (_sequencedata.TimeCount1 - _sequencedata.StartTime);
		//TimeTotal is used for each individual section within the sequence
		//there are 2 sections within this sequence
		double TimeTotal = 2*(_sequencedata.TimeTotal / NoPins);

		/*if (TimeLapse < TimeTotal) { ChaseCalc(_sequencedata, 0, TimeTotal,TimeLapse); }
		if (TimeLapse < ((TimeTotal / 2) + (1*TimeTotal)) & TimeLapse >(TimeTotal / 2)) { ChaseCalc(_sequencedata, 1, TimeTotal, TimeLapse); }
		if (TimeLapse < ((TimeTotal / 2) + (2 * TimeTotal)) & TimeLapse >((TimeTotal / 2) + (1 * TimeTotal))) { ChaseCalc(_sequencedata, 2, TimeTotal, TimeLapse); }
		if (TimeLapse < ((TimeTotal / 2) + (3 * TimeTotal)) & TimeLapse >((TimeTotal / 2) + (2 * TimeTotal))) { ChaseCalc(_sequencedata, 3, TimeTotal, TimeLapse); }
		if (TimeLapse < ((TimeTotal / 2) + (4 * TimeTotal)) & TimeLapse >((TimeTotal / 2) + (3 * TimeTotal))) { ChaseCalc(_sequencedata, 4, TimeTotal, TimeLapse); }
		if (TimeLapse < ((TimeTotal / 2) + (5 * TimeTotal)) & TimeLapse >((TimeTotal / 2) + (4 * TimeTotal))) { ChaseCalc(_sequencedata, 5, TimeTotal, TimeLapse); }
		if (TimeLapse < ((TimeTotal / 2) + (6 * TimeTotal)) & TimeLapse >((TimeTotal / 2) + (5 * TimeTotal))) { ChaseCalc(_sequencedata, 6, TimeTotal, TimeLapse); }
		if (TimeLapse < ((TimeTotal / 2) + (7 * TimeTotal)) & TimeLapse >((TimeTotal / 2) + (6 * TimeTotal))) { ChaseCalc(_sequencedata, 7, TimeTotal, TimeLapse); }
		if (TimeLapse < ((TimeTotal / 2) + (8 * TimeTotal)) & TimeLapse >((TimeTotal / 2) + (7 * TimeTotal))) { ChaseCalc(_sequencedata, 8, TimeTotal, TimeLapse); }
		if (TimeLapse < ((TimeTotal / 2) + (9 * TimeTotal)) & TimeLapse >((TimeTotal / 2) + (8 * TimeTotal))) { ChaseCalc(_sequencedata, 9, TimeTotal, TimeLapse); }
		if (TimeLapse < ((TimeTotal / 2) + (10 * TimeTotal)) & TimeLapse >((TimeTotal / 2) + (9 * TimeTotal))) { ChaseCalc(_sequencedata, 10, TimeTotal, TimeLapse); }
		if (TimeLapse < ((TimeTotal / 2) + (11 * TimeTotal)) & TimeLapse >((TimeTotal / 2) + (10 * TimeTotal))) { ChaseCalc(_sequencedata, 11, TimeTotal, TimeLapse); }
		if (TimeLapse < ((TimeTotal / 2) + (12 * TimeTotal)) & TimeLapse >((TimeTotal / 2) + (11 * TimeTotal))) { ChaseCalc(_sequencedata, 12, TimeTotal, TimeLapse); }
		if (TimeLapse < ((TimeTotal / 2) + (13 * TimeTotal)) & TimeLapse >((TimeTotal / 2) + (12 * TimeTotal))) { ChaseCalc(_sequencedata, 13, TimeTotal, TimeLapse); }
		if (TimeLapse < ((TimeTotal / 2) + (14 * TimeTotal)) & TimeLapse >((TimeTotal / 2) + (13 * TimeTotal))) { ChaseCalc(_sequencedata, 14, TimeTotal, TimeLapse); }
		if (TimeLapse < ((TimeTotal / 2) + (15 * TimeTotal)) & TimeLapse >((TimeTotal / 2) + (14 * TimeTotal))) { ChaseCalc(_sequencedata, 15, TimeTotal, TimeLapse); }*/


	}

	SequenceData ChaseCalc(SequenceData _sequencedata, int i, double TimeTotal, double _TimeLapse )
	{
		double MaxPVal = ConvertPercentageToPWM(_sequencedata.PWMMaxPercent);
		double MinPVal = ConvertPercentageToPWM(_sequencedata.PWMMinPercent);
		double TimeLapse = _TimeLapse - ((TimeTotal / 2) + ((i - 1) * TimeTotal));

		if (TimeTotal < TimeTotal / 2)
		{	//calculate the current pwm level to be used based on time lapsed so far in sequence
			double PWM = (MinPVal + (((MaxPVal - MinPVal) / (TimeTotal / 10000)) * round(TimeLapse / 10000)));

			int PWMOut = GammaCorrection(PWM);
			Serial.print("fadeUp chase pwmout= "); Serial.println(PWMOut);
			_sequencedata.PWMwriteVal[i] = PWMOut;

			return _sequencedata;
		}

		if (TimeTotal > TimeTotal / 2)
		{	//calculate the current pwm level to be used based on time lapsed so far in sequence

			double PWM = (MaxPVal - (((MaxPVal - MinPVal) / (TimeTotal / 10000)) * round((TimeLapse - TimeTotal) / 10000)));
			int PWMOut = GammaCorrection(PWM);
			Serial.print("fadeDown chase pwmout= "); Serial.println(PWMOut);
			_sequencedata.PWMwriteVal[i] = PWMOut;

			return _sequencedata;
		}
	}
};

//set up unique instances of the the functional code to run multiple light effects at the same time
//16 sequences so that each light can be controlled individually
SequenceFunctions SF1;
SequenceFunctions SF2;
SequenceFunctions SF3;
SequenceFunctions SF4;
SequenceFunctions SF5;
SequenceFunctions SF6;
SequenceFunctions SF7;
SequenceFunctions SF8;
SequenceFunctions SF9;
SequenceFunctions SF10;
SequenceFunctions SF11;
SequenceFunctions SF12;
SequenceFunctions SF13;
SequenceFunctions SF14;
SequenceFunctions SF15;
SequenceFunctions SF16;

//set up command data sets
//It is possible to control each individual light with 16 commands
SequenceData cmd1;
SequenceData cmd2;
SequenceData cmd3;
SequenceData cmd4;
SequenceData cmd5;
SequenceData cmd6;
SequenceData cmd7;
SequenceData cmd8;
SequenceData cmd9;
SequenceData cmd10;
SequenceData cmd11;
SequenceData cmd12;
SequenceData cmd13;
SequenceData cmd14;
SequenceData cmd15;
SequenceData cmd16;




void _pwmWrite(int LightSet[], int PWMwriteVal[] )//external analogue write as this retains the pwm value?
{
	//Serial.println("PWMwrite");
	//work out the number of elements in Pin[], loop through pin until 0 is found. 0 must be avoided as a pin number otherwise it doesnt work.
	int x = 0;
	while (LightSet[x] != 0)
	{
		x++;
	}
	int PinSize = x;

	for (int j = 0; j < PinSize; j++)
	{
		analogWrite(LightSet[j], PWMwriteVal[j]);// for all the pins in the set correct the gamma level and set the pwm value for the pin

	}
	
}


// the setup function runs once when you press reset or power the board
void setup() 
{
	//set the frequency based on the number of required PWM bits
	analogWriteResolution(PWMNumberOfBits);
	SequenceNumber = 0;
	Serial.begin(115200);

	
	//test set up only
	cmd1.FunctionType = 3;
	cmd1.LightSet[0] = 8;
	cmd1.LightSet[1] = 7;
	cmd1.LightSet[2] = 9;
	cmd1.LightSet[3] = 10;
	cmd1.PWMMaxPercent = 100;
	cmd1.PWMMinPercent = 0;
	cmd1.Repeat = "Yes";
	cmd1.TimeTotal = 5000000; //micro secconds

	cmd2.FunctionType = 4;
	cmd2.LightSet[0] = 21;
	cmd2.LightSet[1] = 22;
	cmd2.LightSet[2] = 20;
	cmd2.LightSet[3] = 14;
	cmd2.PWMMaxPercent = 100;
	cmd2.PWMMinPercent = 0;
	cmd2.Repeat = "Yes";
	cmd2.TimeTotal = 2 * 1000 * 1000; //3 seconds multiplied to get Milli then micro
	
	cmd3.FunctionType = 3;
	cmd3.LightSet[0] = 35;
	cmd3.LightSet[1] = 36;
	cmd3.LightSet[2] = 38;
	cmd3.LightSet[3] = 37;
	cmd3.PWMMaxPercent = 100;
	cmd3.PWMMinPercent = 0;
	cmd3.Repeat = "Yes";
	cmd3.TimeTotal = 2 * 1000 * 1000; //3 seconds multiplied to get Milli then micro

	cmd4.FunctionType = 5;
	cmd4.LightSet[0] = 2;
	cmd4.LightSet[1] = 5;
	cmd4.LightSet[2] = 6;
	cmd4.PWMMaxPercent = 100;
	cmd4.PWMMinPercent = 0;
	cmd4.Repeat = "Yes";
	cmd4.TimeTotal = 1 * 1000 * 1000; //3 seconds multiplied to get Milli then micro

	cmd5.FunctionType = 0;
	cmd5.LightSet[0] = 29;
	cmd5.PWMwriteVal[0] = 4095;
	cmd5.Repeat = "Yes";

}

// the loop function runs over and over again until power down or reset
void loop() 
{


	cmd1 = SF1.RunSequence(cmd1);
	cmd2 = SF2.RunSequence(cmd2);
	cmd3 = SF2.RunSequence(cmd3);
	cmd4 = SF2.RunSequence(cmd4);
	cmd5 = SF2.RunSequence(cmd5);
	cmd6 = SF2.RunSequence(cmd6);
	cmd7 = SF2.RunSequence(cmd7);
	cmd8 = SF2.RunSequence(cmd8);
	cmd9 = SF2.RunSequence(cmd9);
	cmd10 = SF2.RunSequence(cmd10);
	cmd11 = SF2.RunSequence(cmd11);
	cmd12 = SF2.RunSequence(cmd12);
	cmd13 = SF2.RunSequence(cmd13);
	cmd14 = SF2.RunSequence(cmd14);
	cmd15 = SF2.RunSequence(cmd15);
	cmd16 = SF2.RunSequence(cmd16);

	_pwmWrite(cmd1.LightSet,cmd1.PWMwriteVal);
	_pwmWrite(cmd2.LightSet,cmd2.PWMwriteVal);
	_pwmWrite(cmd3.LightSet, cmd3.PWMwriteVal);
	_pwmWrite(cmd4.LightSet, cmd4.PWMwriteVal);
	_pwmWrite(cmd5.LightSet, cmd5.PWMwriteVal);
	_pwmWrite(cmd6.LightSet, cmd6.PWMwriteVal);
	_pwmWrite(cmd7.LightSet, cmd7.PWMwriteVal);
	_pwmWrite(cmd8.LightSet, cmd8.PWMwriteVal);
	_pwmWrite(cmd9.LightSet, cmd9.PWMwriteVal);
	_pwmWrite(cmd10.LightSet, cmd10.PWMwriteVal);
	_pwmWrite(cmd11.LightSet, cmd11.PWMwriteVal);
	_pwmWrite(cmd12.LightSet, cmd12.PWMwriteVal);
	_pwmWrite(cmd13.LightSet, cmd13.PWMwriteVal);
	_pwmWrite(cmd14.LightSet, cmd14.PWMwriteVal);
	_pwmWrite(cmd15.LightSet, cmd15.PWMwriteVal);
	_pwmWrite(cmd16.LightSet, cmd16.PWMwriteVal);


}
